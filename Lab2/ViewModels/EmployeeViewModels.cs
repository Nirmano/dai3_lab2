﻿using Lab2.Models;
using Lab2.Services;
using Lab2.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab2.ViewModels
{
    public class EmployeeViewModels : ObservableObject
    {
        private EmployeeModel _selectedEmployee;
        public EmployeeModel SelectedEmployee
        {
            get { return _selectedEmployee; }
            set { OnPropertyChanged(ref _selectedEmployee, value); }
        }

        private bool _isEditMode;
        public bool IsEditMode
        {
            get { return _isEditMode; }
            set
            {
                OnPropertyChanged(ref _isEditMode, value);
                OnPropertyChanged("IsDisplayMode");
            }
        }

        public bool IsDisplayMode
        {
            get { return !_isEditMode; }
        }

        public ObservableCollection<EmployeeModel> Employees { get; private set; }

        public ICommand EditCommand { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ICommand UpdateCommand { get; private set; }
        public ICommand BrowseImageCommand { get; private set; }
        public ICommand AddCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        private IDataService<EmployeeModel> _dataService;
        private IDialogService _dialogService;

        public EmployeeViewModels(IDataService<EmployeeModel> dataService, IDialogService dialogService)
        {
            _dataService = dataService;
            _dialogService = dialogService;

            EditCommand = new RelayCommand(Edit, CanEdit);
            SaveCommand = new RelayCommand(Save, IsEdit);
            UpdateCommand = new RelayCommand(Update);
            AddCommand = new RelayCommand(Add);
            DeleteCommand = new RelayCommand(Delete, CanDelete);
        }

        private void Delete()
        {
            Employees.Remove(SelectedEmployee);
            Save();
        }

        private bool CanDelete()
        {
            return SelectedEmployee == null ? false : true;
        }

        private void Add()
        {
            var newEmployee = new EmployeeModel
            {
                Name = "N/A",
                Surname = "N/A",
                CardNumber = "N/A",
                EmployeeNumber = "N/A",
            };

            Employees.Add(newEmployee);
            SelectedEmployee = newEmployee;
        }

        private void Update()
        {
            _dataService.Save(Employees);
        }

        private void Save()
        {
            _dataService.Save(Employees);
            IsEditMode = false;
            OnPropertyChanged("SelectedEmployee");
        }

        private bool IsEdit()
        {
            return IsEditMode;
        }

        private bool CanEdit()
        {
            if (SelectedEmployee == null)
                return false;

            return !IsEditMode;
        }

        private void Edit()
        {
            IsEditMode = true;
        }

        public void LoadEmployee(IEnumerable<EmployeeModel> employees)
        {
            Employees = new ObservableCollection<EmployeeModel>(employees);
            OnPropertyChanged("Employees");
        }
    }
}
