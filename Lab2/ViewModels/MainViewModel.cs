﻿using Lab2.Models;
using Lab2.Services;
using Lab2.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Lab2.ViewModels
{
    public class MainViewModel : ObservableObject
    {
        private IDataService<EmployeeModel> _dataService;
        private IDialogService _dialogService;

        private EmployeeViewModels _employeeVM;
        public EmployeeViewModels EmployeeVM
        {
            get { return _employeeVM; }
            set { OnPropertyChanged(ref _employeeVM, value); }
        }

        public ICommand LoadEmployeeCommand { get; private set; }
        public ICommand LoadFavoritesCommand { get; private set; }

        public MainViewModel(IDataService<EmployeeModel> dataService, IDialogService dialogService)
        {
            EmployeeVM = new EmployeeViewModels(dataService, dialogService);

            _dataService = dataService;

            LoadEmployeeCommand = new RelayCommand(LoadEmployee);
        }

        private void LoadEmployee()
        {
            EmployeeVM.LoadEmployee(_dataService.GetEmployees());
        }
    }
}
