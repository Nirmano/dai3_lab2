﻿using Lab2.Services;
using Lab2.ViewModels;
using Lab2.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class AppViewModel : ObservableObject
    {
        private object _currentView;
        public object CurrentView
        {
            get { return _currentView; }
            set { OnPropertyChanged(ref _currentView, value); }
        }

        private MainViewModel _mainVM;
        public MainViewModel MainVM
        {
            get { return _mainVM; }
            set { OnPropertyChanged(ref _mainVM, value); }
        }

        public AppViewModel()
        {
            var dataService = new MockDataServiceEmployee();
            var dialogService = new WindowDialogService();

            MainVM = new MainViewModel(dataService, dialogService);
            CurrentView = MainVM;
        }
    }
}
