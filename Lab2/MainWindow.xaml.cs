﻿using Lab2.Services;
using Lab2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Show_Room(object sender, RoutedEventArgs e)
        {
            DataContext = new RoomViewModels();
        }
        private void Add_Room(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Add Room");
        }
        private void Modifie_Room(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Modifie Room");
        }
        private void Delete_Room(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Delete Room");
        }
  /*      private void Show_Employee(object sender, RoutedEventArgs e)
        {
            var dataService = new MockDataServiceEmployee();
            var dialogService = new WindowDialogService();


            DataContext = new MainViewModel(dataService,dialogService);
        }
        */
        private void Add_Employee(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Add Employee");
        }
        private void Modifie_Employee(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Modifie Employee");
        }
        private void Delete_Employee(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Delete Employee");
        }
    }
}
