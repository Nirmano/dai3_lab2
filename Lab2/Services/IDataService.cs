﻿using Lab2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Services
{
    public interface IDataService<t>
    {
        IEnumerable<t> GetEmployees();
        void Save(IEnumerable<t> employees);
    }
}
