﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2.Models;

namespace Lab2.Services
{
    public class MockDataServiceEmployee : IDataService<EmployeeModel>
    {

        private IEnumerable<EmployeeModel> _employees;

        public MockDataServiceEmployee()
        {
            _employees = new List<EmployeeModel>()
            {
                new EmployeeModel
                {
                    CardNumber = "0000000001",
                    EmployeeNumber = "0000000001",
                    Name = "Reucherand",
                    Surname = "Anthony"
                },
                new EmployeeModel
                {
                    CardNumber = "0000000002",
                    EmployeeNumber = "0000000002",
                    Name = "Reucherand",
                    Surname = "Arwen"
                },
                new EmployeeModel
                {
                    CardNumber = "0000000003",
                    EmployeeNumber = "0000000003",
                    Name = "Lavallée",
                    Surname = "Nolan"
                },
                new EmployeeModel
                {
                    CardNumber = "0000000004",
                    EmployeeNumber = "0000000004",
                    Name = "Lavallée-Lagotte",
                    Surname = "Jessica"
                }
            };
        }

        public IEnumerable<EmployeeModel> GetEmployees()
        {
            return _employees;
        }

        public void Save(IEnumerable<EmployeeModel> employees)
        {
            _employees = employees;
        }
    }
}
